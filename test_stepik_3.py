import pytest
from stepik3 import add
import pytest

def test_add():
    assert add(2, 3) == 5
    assert add(1, 1) == 2
    assert add(-1, 1) == 0
    assert add(-1, -1) == -2
